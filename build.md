## Docker Compose（推荐）

Compose 是用于定义和运行多容器 Docker 应用程序的工具。通过 Compose，您可以使用 YML 文件来配置应用程序需要的所有服务。然后，使用一个命令，就可以从 YML 文件配置中创建并启动所有服务。
使用之前需要先安装docker环境，建议版本为17.06.0-ce以上版本
1. 下载源码，安装maven环境，
   打包项目 `mvn clean package -Dmaven.test.skip=true -Pdev`，放到服务器
2. 进入 `docs/docker` 目录
3. 按照注释修改 `.env` 文件s
4. 执行 `docker-compose -p oneblog up -d` ,要更新的话 需要执行 `docker-compose down` `docker-compose -p oneblog up -d --build` 
5. 在需要发布的工程中执行 `docker build -t registry.cn-beijing.aliyuncs.com/bbattr/blog-web:3.2.5.14 .` 
6. 发布 `docker push registry.cn-beijing.aliyuncs.com/bbattr/blog-web:3.2.5.14`
