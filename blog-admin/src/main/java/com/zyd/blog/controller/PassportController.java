package com.zyd.blog.controller;

import com.zyd.blog.business.annotation.BussinessLog;
import com.zyd.blog.business.entity.User;
import com.zyd.blog.business.entity.UserPwd;
import com.zyd.blog.business.service.PassportService;
import com.zyd.blog.business.service.SysUserService;
import com.zyd.blog.dto.LoginRequest;
import com.zyd.blog.dto.LoginResponse;
import com.zyd.blog.framework.holder.RequestHolder;
import com.zyd.blog.framework.object.ResponseVO;
import com.zyd.blog.framework.property.AppProperties;
import com.zyd.blog.util.JwtTokenUtil;
import com.zyd.blog.util.ResultUtil;
import com.zyd.blog.util.SessionUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.SavedRequest;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;

/**
 * 登录相关
 * <a href="https://www.letcode.cn">...</a>
 *
 * @author jack.chen
 * @since 1.0 2018/4/24 14:37
 */
@Slf4j
@Controller
@RequestMapping(value = "/passport")
@CrossOrigin(origins = "*")
public class PassportController {

    @Resource
    private AppProperties config;
    @Resource
    private SysUserService userService;
    @Resource
    private PassportService passportService;


    /**
     * 登录
     *
     * @param request 登录
     */
    @BussinessLog("[{1}]登录系统")
    @PostMapping("/loginByPassword")
    @ResponseBody
    public ResponseVO<LoginResponse> loginByPassword(
            @RequestBody LoginRequest request) {
        if (config.isEnableCaptcha()) {
            // TODO 跨域的验证代码的方式要进行更换
            if (StringUtils.isEmpty(request.getCaptchaId()) || !request.getCaptchaId().equals(SessionUtil.getCaptcha())) {
                return ResultUtil.error("验证码错误！");
            }
            SessionUtil.removeKaptcha();
        }
        User user = passportService.login(request.getUsername(), request.getPassword());
        try {
            String token = JwtTokenUtil.creatToken(request.getUsername(), user.getId(), 4);
            // 返回的登录后的验证token
            return ResultUtil.success(LoginResponse.builder().token(token).build());
        } catch (Exception e) {
            log.error("登录失败，用户名 [{}]：{}", request.getUsername(), e.getMessage());
            return ResultUtil.error(e.getMessage());
        }
    }


    /// ===========================================================================

    @BussinessLog("进入登录页面")
    @GetMapping("/login")
    public ModelAndView login(Model model) {
        model.addAttribute("enableCaptcha", config.isEnableCaptcha());
        return ResultUtil.view("/login");
    }

    /**
     * 登录
     *
     * @param username
     * @param password
     * @return
     */
    @BussinessLog("[{1}]登录系统")
    @PostMapping("/signin")
    @ResponseBody
    public ResponseVO<String> submitLogin(String username, String password, boolean rememberMe, String kaptcha) {
        if (config.isEnableCaptcha()) {
            if (StringUtils.isEmpty(kaptcha) || !kaptcha.equals(SessionUtil.getCaptcha())) {
                return ResultUtil.error("验证码错误！");
            }
            SessionUtil.removeKaptcha();
        }
        UsernamePasswordToken token = new UsernamePasswordToken(username, password, rememberMe);
        //获取当前的Subject
        Subject currentUser = SecurityUtils.getSubject();
        try {
            // 在调用了login方法后,SecurityManager会收到AuthenticationToken,并将其发送给已配置的Realm执行必须的认证检查
            // 每个Realm都能在必要时对提交的AuthenticationTokens作出反应
            // 所以这一步在调用login(token)方法时,它会走到xxRealm.doGetAuthenticationInfo()方法中,具体验证方式详见此方法
            currentUser.login(token);
            SavedRequest savedRequest = WebUtils.getSavedRequest(RequestHolder.getRequest());
            String historyUrl = null;
            if (null != savedRequest) {
                if (!savedRequest.getMethod().equals("POST")) {
                    historyUrl = savedRequest.getRequestUrl();
                }
            }
            return ResultUtil.success(null, historyUrl);
        } catch (Exception e) {
            log.error("登录失败，用户名[{}]：{}", username, e.getMessage());
            token.clear();
            return ResultUtil.error(e.getMessage());
        }
    }

    /**
     * 修改密码
     *
     * @return
     */
    @BussinessLog("修改密码")
    @PostMapping("/updatePwd")
    @ResponseBody
    public ResponseVO updatePwd(@Validated UserPwd userPwd, BindingResult bindingResult) throws Exception {
        if (bindingResult.hasErrors()) {
            return ResultUtil.error(bindingResult.getFieldError().getDefaultMessage());
        }
        boolean result = userService.updatePwd(userPwd);
        SessionUtil.removeAllSession();
        return ResultUtil.success(result ? "密码已修改成功，请重新登录" : "密码修改失败");
    }

    /**
     * 使用权限管理工具进行用户的退出，跳出登录，给出提示信息
     *
     * @param redirectAttributes
     * @return
     */
    @BussinessLog("退出系统")
    @GetMapping("/logout")
    public ModelAndView logout(RedirectAttributes redirectAttributes) {
        // http://www.oschina.net/question/99751_91561
        // 此处有坑： 退出登录，其实不用实现任何东西，只需要保留这个接口即可，也不可能通过下方的代码进行退出
        // SecurityUtils.getSubject().logout();
        // 因为退出操作是由Shiro控制的
        redirectAttributes.addFlashAttribute("message", "您已安全退出");
        return ResultUtil.redirect("index");
    }
}
