#！/bin/bash
# 首先是更新代码
currentDir="$(pwd)"
cd ../../
git pull
mvn clean package -Dmaven.test.skip=true -Pprod
# 停掉服务
cd "$currentDir" || exit
docker-compose down
# 更新启动程序，所有的有变动的容器全部更新
docker-compose -p oneblog up -d --build