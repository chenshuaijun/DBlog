<#include "include/macros.ftl">
<@compress single_line=false>
    <@header title="关于 | ${config.siteName}" description="关于${config.siteName}" canonical="/about"></@header>

    <div class="container custome-container">
        <nav class="breadcrumb">
            <a class="crumbs" title="返回首页" href="${config.siteUrl}" data-toggle="tooltip" data-placement="bottom"><i
                        class="fa fa-home"></i>首页</a>
            <i class="fa fa-angle-right"></i>关于
        </nav>
        <div class="row about-body">
            <@blogHeader title="关于本站"></@blogHeader>
            <#if config.aboutMeHtml?? && (config.aboutMeHtml?length > 0)>
                ${config.aboutMeHtml!}
            <#else >
                <div class="col-sm-12 blog-main">
                    <div class="blog-body expansion">
                        <h5 class="custom-title"><i
                                    class="fa fa-copyright fa-fw"></i><strong>关于博客</strong><small></small></h5>
                        <div class="info">
                            本站所有标注为原创的文章，转载请标明出处。<br>
                            本站所有转载的文章，一般都会在文章中注明原文出处。<br>
                            所有转载的文章皆来源于互联网，若因此对原作者造成侵权，烦请原作者<a target="_blank"
                                                                                             href="mailto:${config.authorEmail}"
                                                                                             title="点击给我发邮件"
                                                                                             data-toggle="tooltip"
                                                                                             data-placement="bottom"
                                                                                             rel="external nofollow"><strong>告知</strong></a>，我会立刻删除相关内容。
                        </div>
                        <#-- <@praise></@praise> -->
                    </div>
                </div>
            <#--            <div class="col-sm-12 blog-main">-->
            <#--                <div class="blog-body expansion">-->
            <#--                    <h5 class="custom-title"><i class="fa fa-coffee fa-fw"></i><strong>简单的要求</strong><small> - 简单点，说话的方式简单点</small></h5>-->
            <#--                    <div class="link-info">-->
            <#--                        <ul class="list-unstyled">-->
            <#--                            <li>本站定位：IT技术男们总结的博客</li>-->
            <#--                            <li>本站作用：写博客、记日志、闲聊扯淡鼓捣技术</li>-->
            <#--                            <li>本站 <em>优先</em> 选择<kbd>同类原创、内容相近</kbd>的博客或网站，您的站点内容可以为<kbd>技术类</kbd>、<kbd>IT科技</kbd>、<kbd>互联网</kbd>和<kbd>生活</kbd></li>-->
            <#--                            <li>特别提醒：<abbr title="禁止友链的网站">任何包含违反国家法律法规内容禁止在网站上发布</abbr></li>-->
            <#--                            <li>-->
            <#--                                <blockquote>-->
            <#--                                    <ul class="list-unstyled">-->
            <#--                                        <li>复制添加本站链接：<strong>&lt;a href="${config.siteUrl}" title="${config.siteDesc}" target="_blank"&gt;${config.siteName}&lt;/a&gt;</strong></li>-->
            <#--                                        <li>复制添加本站图标：<strong>${config.siteFavicon}</strong></li>-->
            <#--                                    </ul>-->
            <#--                                </blockquote>-->
            <#--                            </li>-->
            <#--                            <li><a class="btn btn-default btn-sm auto-link-btn" style="margin-top: 10px;" data-toggle="modal" data-target="#auto-link-modal"><i class="fa fa-hand-grab-o"></i>自助申请友链</a></li>-->
            <#--                        </ul>-->
            <#--                    </div>-->
            <#--                </div>-->
            <#--            </div>-->
            </#if>
            <#--<div class="col-sm-12 blog-main">
                <div class="blog-body expansion">
                    <div id="comment-box" data-id="-3"></div>
                </div>
            </div>-->
        </div>
    </div>



    <!--评论弹框-->
    <div class="modal fade bs-example-modal-sm" id="auto-link-modal" tabindex="-1" role="dialog"
         aria-labelledby="auto-link-modal-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="auto-link-modal-label"><i class="fa fa-spinner fa-fw fa-spin"></i>自助申请友情链接
                    </h4>
                </div>
                <div class="modal-body" style="overflow: hidden">
                    <div class="alert alert-warning" role="alert">
                        <i class="fa fa-lightbulb-o"></i>友情提示：提交前请先添加本站友链，确认添加后再次提交申请，系统将自动审核！
                    </div>
                    <form class="form-horizontal col-sm-12" role="form" action="/api/autoLink" method="post"
                          id="autoLinkForm" style="padding-left: 15px;">
                        <div class="form-group has-info">
                            <div class="input-group input-logo">
                                <input type="text" class="form-control" name="name" placeholder="${config.siteName}"
                                       style="padding-left: 88px;">
                                <span class="fa fa-user pull-left" aria-hidden="true"
                                      style="width: 75px;">站点名称</span>
                            </div>
                        </div>
                        <div class="form-group has-info">
                            <div class="input-group input-logo">
                                <input type="text" class="form-control" name="url"
                                       placeholder="本站链接所在的网页(内页或首页)" style="padding-left: 88px;">
                                <span class="fa fa-map-marker pull-left" aria-hidden="true"
                                      style="width: 75px;">站点地址</span>
                            </div>
                        </div>
                        <div class="form-group has-info">
                            <div class="input-group input-logo">
                                <input type="text" class="form-control" name="description"
                                       placeholder="一个程序员的个人博客。心之所向，无所不能。"
                                       style="padding-left: 88px;">
                                <span class="fa fa-commenting pull-left" aria-hidden="true"
                                      style="width: 75px;">站点描述</span>
                            </div>
                        </div>
                        <div class="form-group has-info">
                            <div class="input-group input-logo">
                                <input type="text" class="form-control" name="email" placeholder="选填"
                                       style="padding-left: 88px;">
                                <span class="fa fa-envelope pull-left" aria-hidden="true"
                                      style="width: 75px;">站长邮箱</span>
                            </div>
                        </div>
                        <div class="form-group has-info">
                            <div class="input-group input-logo">
                                <input type="text" class="form-control" name="favicon" placeholder="选填"
                                       style="padding-left: 88px;">
                                <span class="fa fa-file-image-o pull-left" aria-hidden="true"
                                      style="width: 75px;">站点图标</span>
                            </div>
                        </div>
                        <div class="form-group has-info">
                            <button type="button" class="btn btn-default btn-sm autoLink"
                                    data-loading-text="正在提交申请，请稍后..."><i class="fa fa-smile-o"></i>提交申请
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>




    <@footer>
        <#if (config.enableHitokoto == 1 || config.enableHitokoto == "1")>
            <script src="https://v1.hitokoto.cn/?encode=js&c=i&select=.hitokoto" defer></script>
        </#if>
    </@footer>
</@compress>
