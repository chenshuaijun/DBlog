<p align="center">
	<img alt="" src="/docs/_media/logo.png" style="width: 300px"></img>
</p>
<p align="center">
	<strong>OneBlog 一个简洁美观、功能强大并且自适应的Java博客。使用springboot开发，前端使用Bootstrap。支持移动端自适应，配有完备的前台和后台管理功能。</strong>
</p>
<p align="center">
	<img alt="" src="https://img.shields.io/badge/Maven-3.3.1-green.svg" ></img>
	<a target="_blank" href="https://gitee.com/yadong.zhang/DBlog/blob/master/LICENSE">
		<img alt="" src="https://img.shields.io/badge/license-GPL%20v3-yellow.svg" ></img>
	</a>
	<a target="_blank" href="https://www.oracle.com/technetwork/java/javase/downloads/index.html">
		<img alt="" src="https://img.shields.io/badge/JDK-1.8+-blue.svg" ></img>
	</a>
	<img alt="" src="https://img.shields.io/badge/MySQL-5.6.4-red.svg" ></img>
	<img alt="" src="https://img.shields.io/badge/Redis-3.0.503-orange.svg" ></img>
</p>
<p align="center">
	<strong>开源地址：</strong> <a target="_blank" href='https://gitee.com/yadong.zhang/DBlog'>Gitee</a> | <a target="_blank" href='https://github.com/zhangyd-c/OneBlog'>Github</a>
</p>
<p align="center">
    <strong>QQ群：</strong>230017570
</p>

----

# 写在前面的话
ps: 虽然我知道，大部分人都是来了**直接下载源代码**后就潇洒的离开，并且只有等到下次**突然想到**“我天~~我得去看看OneBlog这烂项目更新新功能了吗”的时候才会重新来到这儿，即使你重新来过，我估计你也只有两个选择：    

发现更新代码了 --> 下载源码后重复上面的步骤    
发现没更新代码 --> 直接关闭浏览器

虽然我知道现实就是如此的残酷，但我还是要以我萤虫之力对各位到来的同仁发出一声诚挚的嘶吼：

**如果喜欢，请多多分享！！多多Star！！**

----


# 功能简介

- **Docker一键部署**：支持 Docker 的方式一键启动服务
- **广告位管理**：支持五种广告位：首页开屏广告、侧边栏顶部、侧边栏底部、文章详情底部、评论框顶部，站长可以随时随意更换自己的广告链接，赚外快不成问题！
- **多种编辑器**：支持 wangEditor、Markdown 和 TinyMCE 等多种文章编辑器，可以自行选择
- **自动申请友情链接**：在线申请友情链接，无需站长手动配置，只需申请方添加完站长的连接后自行申请即可
- **百度推送**：支持百度推送功能，加速百度搜索引擎收录博文
- **评论系统**：自研的评论系统，支持显示用户地址、浏览器和 os 信息，后台可审核评论、开启匿名评论、回复和邮件通知评论
- **权限管理**：后台配备完善的 RBAC 权限管理，前台文章支持密码访问、登录访问等多种权限验证策略
- **完善的 SEO 方案**：自带robots、sitemap 等 seo 模板，实现自动生成 robots 和 sitemap
- **实时通讯**：管理员可向在线的用户实时发送消息
- **系统配置支持快速配置**：可通过后台手动修改诸如域名信息、SEO 优化、赞赏码、七牛云以及更新维护通知等
- **多种文件存储**：集成七牛云、阿里云OSS，实现文件云存储，同时支持本地文件存储
- **文章搬运工**：集成[blog-hunter](https://gitee.com/yadong.zhang/blog-hunter) 实现“文章搬运工”功能，支持一键同步imooc、csdn、iteye或者cnblogs上的文章，可抓取列表和单个文章
- **第三方授权登录**：集成 [JustAuthPlus（JAP）](https://gitee.com/fujieid/jap) 实现第三方授权登录
- **自定义网站内容**：管理员可自定义“关于本站”、“留言板”、“友情链接”、“免责声明”、“Footer”、“鼠标点击时的气泡文字”、“热门搜索的待选项”等内容
- **自定义页面**：管理员可添加自定义的页面
- **流控**：针对异常IP的连续大量访问，系统会自动封禁该IP。

----

# 模块划分

| 模块  | 释义 | 备注 |
| :------------: | :------------: | :------------: |
| blog-core | 核心业务类模块，提供基本的数据操作、工具处理等 | 该模块只是作为核心依赖包存在 |
| blog-codegen | 代码生成器 |
| blog-admin | 后台管理模块 | 该模块作为单独项目打包部署 |
| blog-web | 前台模块 | 该模块作为单独项目打包部署 |
| blog-file | 文件存储功能模块 | 支持local、七牛云和阿里云OSS |


# 技术栈

- docker
- docker-compose
- Springboot 2.3.4.RELEASE
- Apache Shiro 1.7.1
- Logback
- Redis
- Lombok
- Websocket
- MySQL、Mybatis、Mapper、Pagehelper
- Freemarker
- Bootstrap 3.3.0
- wangEditor
- Markdown
- jQuery 1.11.1、jQuery Lazyload 1.9.7、fancybox、iCheck
- 阿里云OSS
- Nginx
- kaptcha
- webMagic
- ...


# 快速开始

## Docker Compose（推荐）

Compose 是用于定义和运行多容器 Docker 应用程序的工具。通过 Compose，您可以使用 YML 文件来配置应用程序需要的所有服务。然后，使用一个命令，就可以从 YML 文件配置中创建并启动所有服务。
使用之前需要先安装docker环境，建议版本为17.06.0-ce以上版本
1. 下载源码，安装maven环境，打包项目 `mvn clean package -Dmaven.test.skip=true -Pdev`，放到服务器
2. 进入 `docs/docker` 目录
3. 按照注释修改 `.env` 文件
4. 执行 `docker-compose -p oneblog up -d` ,要更新的话 需要执行 `docker-compose down` `docker-compose -p oneblog up -d --build`

web 发布到k8s的操作命令
1. 执行 `mvn clean package -Dmaven.test.skip=true -Pdev` 
2. 在blog-web文件夹中执行 `docker build . -t registry.cn-beijing.aliyuncs.com/bbattr/blog-web:3.2.5.20`
3. 执行 `docker push registry.cn-beijing.aliyuncs.com/bbattr/blog-web:3.2.5.20` 发布推送
4. 更新K8S的服务 `kubectl set image deployment -n devops blog-web *=registry.cn-beijing.aliyuncs.com/bbattr/blog-web:3.2.5.20`

admin 发布到k8s的操作命令
1. 执行 `mvn clean package -Dmaven.test.skip=true -Pdev` 
2. 在blog-admin文件夹中执行 `docker build . -t registry.cn-beijing.aliyuncs.com/bbattr/blog-admin:3.2.5.22`  
3. `docker tag 392ff6232983 registry.cn-beijing.aliyuncs.com/bbattr/blog-admin:3.2.5.22`
4. 执行 `docker push registry.cn-beijing.aliyuncs.com/bbattr/blog-admin:3.2.5.22` 发布推送
5. 更新K8S的服务 `kubectl set image deployment -n devops blog-admin *=registry.cn-beijing.aliyuncs.com/bbattr/blog-admin:3.2.5.22`

## 源码方式

> `blog-web` 和 `blog-admin` 的运行方式一样 

1. 使用IDE导入本项目
2. 新建数据库`CREATE DATABASE dblog;`
3. 导入数据库`docs/docker/mysql/dblog.sql`
4. 初始化数据库`docs/docker/mysql/init_data.sql`
5. 修改配置文件，mysql、redis、mail配置在`[blog-core]/resources/config/application-center.yml`配置文件中
6. 运行项目：直接运行 `blog-web/src/main/java/com/zyd/blog/BlogWebApplication.java` 或者 `blog-admin/src/main/java/com/zyd/blog/BlogAdminApplication.java`
7. 浏览器访问`http://127.0.0.1:{port}`

[//]: # (> 后台默认账号密码：root/123456)

# Who is using?

烦请各位使用 OneBlog 的朋友，能留下你的网址 - [点这儿](https://gitee.com/yadong.zhang/DBlog/issues/ILIAQ)

----

# 开源协议

[![license](https://img.shields.io/badge/license-GPL%20v3-yellow.svg)](https://gitee.com/yadong.zhang/DBlog/blob/master/LICENSE)
