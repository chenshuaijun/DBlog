package com.zyd.blog.business.service.impl;

import com.zyd.blog.business.entity.User;
import com.zyd.blog.business.enums.UserStatusEnum;
import com.zyd.blog.business.service.PassportService;
import com.zyd.blog.business.service.SysUserService;
import com.zyd.blog.util.PasswordUtil;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 登录验证服务
 * @since 2
 * @author jack.chen
 */
@Service
public class PassportServiceImpl implements PassportService {
    @Resource
    private SysUserService userService;

    /**
     * 登录返回用户信息
     *
     * @param username 用户名
     * @param password 密码
     * @return User
     */
    @Override
    public User login(String username, String password) {
        User user = userService.getByUserName(username);
        if (user == null) {
            throw new UnknownAccountException("账号不存在！");
        }
        if (user.getStatus() != null && UserStatusEnum.DISABLE.getCode().equals(user.getStatus())) {
            throw new LockedAccountException("帐号已被锁定，禁止登录！");
        }
        try {
            String pwd = PasswordUtil.decrypt(user.getPassword(), username);
            if (!password.equals(pwd)) {
                throw new LockedAccountException("帐号或密码正确，请重试！");
            }
        } catch (Exception e) {
            throw new LockedAccountException("帐号或密码正确，请重试！");
        }
        return user;
    }
}
