package com.zyd.blog.business.service;

import com.zyd.blog.business.entity.User;

/**
 * 登录令牌验证服务
 *
 * @author jack.chen
 * @since 2
 */
public interface PassportService {
    /**
     * 登录返回用户信息
     *
     * @param username 用户名
     * @param password 密码
     * @return User
     */
    User login(String username, String password);
}
