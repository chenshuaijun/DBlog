package com.zyd.blog.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequest {
    /**
     * 登录成功后的token
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 验证码ID
     */
    private String captchaId;
}
